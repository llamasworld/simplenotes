package com.naumen.dao;

import com.naumen.model.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gleb Bukov on 05.09.16.
 */
@Repository
public class JdbcNoteDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcNoteDao(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * This is the method to be used to create
     * a record in the Notes table.
     */
    public void create(Note note) {
        String sql = "INSERT INTO Notes (id, Title, Text) VALUES (?, ?, ?);";
        jdbcTemplate.update(sql, note.getId(), note.getTitle(), note.getText());
    }

    /**
     * This method returns one Note from DB
     * corresponding to a passed Note id.
     */
    public Note read(int id) {
        String sql = "SELECT * FROM Notes WHERE Notes.id = ?;";
        return jdbcTemplate.query(sql, new Object[]{id}, new ResultSetExtractor<Note>() {
            @Override
            public Note extractData(ResultSet rs) throws SQLException, DataAccessException {
                rs.next();
                Note note = new Note();
                note.setId(rs.getInt("id"));
                note.setTitle(rs.getString("Title"));
                note.setText(rs.getString("Text"));
                return note;
            }
        });
    }

    /**
     * This is the method to be used to delete
     * a record from the Notes table corresponding
     * to a passed Note id.
     */
    public void delete(int id) {
        String sql = "DELETE FROM Notes WHERE Notes.id = ?;";
        jdbcTemplate.update(sql, new Object[]{id});
    }

    /**
     * This method returns all Note from DB.
     */
    public List<Note> getAll() {
        String sql = "SELECT * FROM Notes;";
        return jdbcTemplate.query(sql, new CustomResultSetExtractor());
    }

    public List<Note> getNotesLikeText(String searchText) {
        String sql = "SELECT * FROM Notes WHERE Notes.Title LIKE ? OR Notes.Text LIKE ?;";
        return jdbcTemplate.query(sql, new Object[]{"%" + searchText + "%", "%" + searchText + "%"}, new CustomResultSetExtractor());
    }

    private class CustomResultSetExtractor implements ResultSetExtractor<List<Note>> {

        @Override
        public List<Note> extractData(ResultSet rs) throws SQLException, DataAccessException {
            List<Note> notes = new ArrayList<>();

            while (rs.next()) {
                Note note = new Note();
                note.setId(rs.getInt("id"));
                note.setTitle(rs.getString("Title"));
                note.setText(rs.getString("Text"));
                notes.add(note);
            }

            return notes;
        }
    }
}
