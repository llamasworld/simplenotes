import com.naumen.dao.JdbcNoteDao;
import com.naumen.model.Note;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Gleb Bukov on 05.09.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-overrides.xml"})
public class JdbcNoteDaoTest {

    private Connection connection;
    @Autowired
    private JdbcNoteDao jdbcNoteDao;
    @Autowired
    private DataSource dataSource;

    @Before
    public void setUp() throws SQLException, IOException {
        this.connection = dataSource.getConnection();

        // create and fill tables from sql script
        try (InputStreamReader inputStreamReader =
                     new InputStreamReader(
                             JdbcNoteDaoTest.class.getClassLoader().getResourceAsStream("h2script.sql"))) {
            RunScript.execute(connection, inputStreamReader);
        }

        connection.close();
    }

    @Test
    public void create() {
        Note note = new Note();
        note.setId(1);
        note.setTitle("Title One For Test");
        note.setText("Some letters for test");

        jdbcNoteDao.create(note);
    }

    @Test
    public void read() {
        Note expected = new Note();
        expected.setId(1);
        expected.setTitle("Title for test");
        expected.setText("Body of the note...");
        jdbcNoteDao.create(expected);

        Note actual = jdbcNoteDao.read(1);

        assertEquals(expected, actual);
    }

    @Test
    public void delete() {
        createSevenNotes();
        List<Note> notes = jdbcNoteDao.getAll();
        assertTrue(notes.size() == 7);

        jdbcNoteDao.delete(1);

        notes = jdbcNoteDao.getAll();
        System.out.println(notes.size());
        assertTrue(notes.size() == 6);
    }

    @Test
    public void getAll() {
        List<Note> notes = jdbcNoteDao.getAll();
        assertTrue(notes.size() == 0);

        createSevenNotes();

        notes = jdbcNoteDao.getAll();
        assertTrue(notes.size() == 7);
    }

    @Test
    public void getNotesLikeText() {
        createSevenNotes();
        List<Note> notes = jdbcNoteDao.getNotesLikeText("Title1");
        assertTrue(notes.size() == 1);
        System.out.println(notes.get(0));
    }

    private void createSevenNotes() {
        for (int i = 0; i < 7; i++) {
            Note note = new Note();
            note.setId(i);
            note.setTitle("Title" + i);
            note.setText("Text text text " + i);
            jdbcNoteDao.create(note);
        }
    }
}
