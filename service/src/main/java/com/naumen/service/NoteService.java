package com.naumen.service;

import com.naumen.dao.JdbcNoteDao;
import com.naumen.model.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Gleb Bukov on 05.09.16.
 */
@Service
public class NoteService {

    @Autowired
    private JdbcNoteDao jdbcNoteDao;

    public NoteService(JdbcNoteDao jdbcNoteDao) {
        this.jdbcNoteDao = jdbcNoteDao;
    }

    @Transactional
    public void addNote(Note note) {
        if (note.getTitle() == null || note.getTitle().length() == 0) {
            int textLength = note.getText().length();
            if (textLength > 20) {
                note.setTitle(note.getText().substring(0, 20) + "...");
            } else {
                note.setTitle(note.getText().substring(0, textLength) + "...");
            }
        }
        if (note.getText().length() > 390) {
            note.setText(note.getText().substring(0, 340) + "... (к сожалению заметка была слишком длинной)");
        } else {
            note.setText(note.getText());
        }
        jdbcNoteDao.create(note);
    }

    public Note getNoteById(int id) {
        return jdbcNoteDao.read(id);
    }

    @Transactional
    public void deleteNoteById(int id) {
        jdbcNoteDao.delete(id);
    }

    public List<Note> getAllNotes() {
        return jdbcNoteDao.getAll();
    }

    public List<Note> findNotesByName(String searchText) {
        return jdbcNoteDao.getNotesLikeText(searchText);
    }
}
