package com.naumen.model;

/**
 * Created by Gleb Bukov on 05.09.16.
 */
public class Note {

    private int id;
    private String title;
    private String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        return text.hashCode() + this.id;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof Note)) {
            return false;
        }
        Note obj = (Note) object;
        return this.id == obj.getId() && this.text.equals(obj.getText());
    }

    @Override
    public String toString() {
        return this.id + " " + this.text;
    }
}
