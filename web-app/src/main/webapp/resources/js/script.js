function confirmBigNote(elem) {
    var text = $('#textArea').val();
    if (text.length < 1) {
        alert("Заметка не может быть пустой!");
        return false;
    }
    if (text.length > 390) {
        return confirm("Сообщение слишком большое, нам придется его немного уменьшить. Вы согласны?");
    } else {
        return true;
    }
}