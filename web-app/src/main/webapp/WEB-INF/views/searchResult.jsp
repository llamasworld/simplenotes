<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 06.09.16
  Time: 10:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="SimpleNote">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>

    <title>Поиск</title>
</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-md-12" style="text-align: center;">
            <a href="/writeNote" class="menu-btn btn btn-default btn-xs">добавить заметку</a>
            <a href="/index.jsp" class="menu-btn btn btn-default btn-xs">поиск заметок</a>
            <hr/>
        </div>

        <ul>
            <c:forEach items="${results}" var="note">
                <li>
                    <span>
                    <a href="/note?id=${note.id}" style="text-decoration: none; color: #303030;">
                        <h2>${note.title}</h2>
                        <c:if test="${fn:length(note.text) > 230}">
                            <p>${fn:substring(note.text, 0, 185)} </p>
                            <p>(подробнее)</p>
                        </c:if>
                        <c:if test="${fn:length(note.text) <= 230}">
                            <p>${note.text}</p>
                        </c:if>
                    </a>
                        <div>
                            <a href="${pageContext.request.contextPath}/removeNote?id=${note.id}" style="color: red;">
                                удалить заметку
                            </a>
                        </div>
                    </span>
                </li>
            </c:forEach>
        </ul>

    </div>
</div>

</body>
</html>
