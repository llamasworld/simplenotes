<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 05.09.16
  Time: 19:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="SimpleNote">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>

    <title>Добро пожаловать</title>
</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <div class="div-logo">
                <img src="resources/img/Naumen-test-task-note-1.png"/>
            </div>

            <div class="main-div input-group">
                <form action="${pageContext.request.contextPath}/search" method="post" class="welcome-page-form">
                    <div class="welcome-page-search-line input-group">
                        <input type="text" name="search" class="form-control" placeholder="Найти заметки...">
                        <div class="input-group-btn">
                            <input type="submit" value="Поиск" class="btn btn-default">
                            <a href="/writeNote" class="btn btn-default" style="background-color: #88d888;">+</a>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>

</body>
</html>
