package com.naumen.controllers;

import com.naumen.model.Note;
import com.naumen.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Gleb Bukov on 05.09.16.
 */
@Controller
public class NoteController {

    @Autowired
    private NoteService noteService;

    @RequestMapping(value = "/writeNote", method = RequestMethod.GET)
    public ModelAndView writeNote() {
        return new ModelAndView("writeNote");
    }

    @RequestMapping(value = "/addNote", method = RequestMethod.POST)
    public String addNote(@RequestParam("title") String title, @RequestParam("text") String text) {
        Note note = new Note();
        note.setTitle(title);
        note.setText(text);
        noteService.addNote(note);
        return "redirect: /search";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ModelAndView search(@RequestParam("search") String searchText) {
        List<Note> notes = noteService.findNotesByName(searchText);
        ModelAndView modelAndView = new ModelAndView("searchResult");
        modelAndView.addObject("results", notes);
        return modelAndView;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ModelAndView showAllNotes() {
        List<Note> notes = noteService.getAllNotes();
        ModelAndView modelAndView = new ModelAndView("searchResult");
        modelAndView.addObject("results", notes);
        return modelAndView;
    }

    @RequestMapping(value = "/note", method = RequestMethod.GET)
    public ModelAndView openNote(@RequestParam("id") int id) {
        Note note = noteService.getNoteById(id);
        ModelAndView modelAndView = new ModelAndView("notePage");
        modelAndView.addObject("note", note);
        return modelAndView;
    }

    @RequestMapping(value = "/removeNote", method = RequestMethod.GET)
    public String removeNote(@RequestParam("id") int id) {
        noteService.deleteNoteById(id);
        return "redirect: /search";
    }
}
