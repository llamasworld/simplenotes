![alt text](screenshots/0_welcome.png "Welcome Page")

# Тестовое задание  

Простой мини сервис позволяющий создать\редактировать\удалять заметки с доступом через WEB интерфейс.  
 
В проекте используется трехуровневая архитектура.  

Основные возможности реализованные на данный момент:  
1. Возможность добавить заметку;  
2. Проверка на наличие основного текста;  
3. Проверка на наличие заголовка;  
4. Проверка на длину заметки;  
5. Поиск заметок по 'заголовкам' и 'самим сообщениям';  
6. Возможность просмотреть все заметки;  
7. Удаление заметок;  
8. Методы DAO слоя тестируются на H2  

Стек технологий: Java, Spring Framework, jQuery, Bootstrap, MySQL, H2, jUnit.  

Welcome page:  
![alt text](screenshots/1_mainPage.png "Welcome page")

Search / all notes page:  
![alt text](screenshots/2_allNotes.png "Search / all notes page")

Add note page:  
![alt text](screenshots/3_addNote.png "Add note page")

**Буков Глеб**